#![deny(deprecated, clippy::all, clippy::pedantic)]

#![cfg_attr(not(feature = "std"), no_std)]

use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[deprecated]
    #[error("...")]
    Deprecated,
}
